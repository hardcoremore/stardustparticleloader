package com.funkypandagame.stardustplayer.project
{
	import com.funkypandagame.stardustplayer.emitter.EmitterValueObject;

	import idv.cjcat.stardustextended.handlers.starling.StardustStarlingRenderer;
	import idv.cjcat.stardustextended.handlers.starling.StarlingParticleHandler;
	import idv.cjcat.stardustextended.particles.Particle;

	public class ProjectValueObject
	{
	    public var version:Number;

		public var emitters:Vector.<EmitterValueObject>;
		
	    public function ProjectValueObject(_version:Number)
	    {
	        version = _version;
			emitters = new Vector.<EmitterValueObject>;
	    }
	
	    public function get numberOfEmitters():int
	    {
	        var numEmitters:uint = 0;

	        for each (var emitter:Object in emitters)
	        {
	            numEmitters++;
	        }

	        return numEmitters;
	    }
	
	    public function get numberOfParticles() : uint
	    {
	        var numParticles:uint = 0;

	        for each (var emVO:EmitterValueObject in emitters)
	        {
	            numParticles += emVO.emitter.numParticles;
	        }

	        return numParticles;
	    }
	
	    /** Removes all particles and puts the simulation back to its initial state. */
	    public function resetSimulation():void
	    {
	        for each (var emitterValueObject:EmitterValueObject in emitters)
	        {
	            emitterValueObject.emitter.reset();
	        }
	    }
	
	    public function set fps(val:Number):void
	    {
	        for each (var emitterValueObject:EmitterValueObject in emitters)
	        {
	            emitterValueObject.emitter.fps = val;
	        }
	    }
	
	    public function get fps():Number
	    {
	        return emitters[0].emitter.fps;
	    }
	
	    /**
	     * The simulation will be unusable after calling this method.
	     * Note that this does *NOT* dispose StarlingHandler's texture, since textures are shared by instances.
	     * To dispose the texture call SimLoader.dispose if you dont want to create more simulations of this type.
	     **/
	    public function destroy():void
	    {
			var emitterValueObject:EmitterValueObject;
			var renderer:StardustStarlingRenderer;
			var len:int = emitters.length;
			
	        while(--len > -1)
	        {
				emitterValueObject = emitters.pop();

	            emitterValueObject.emitter.clearParticles();
	            emitterValueObject.emitter.clearActions();
	            emitterValueObject.emitter.clearInitializers();
	            emitterValueObject.emitterSnapshot = null;
				
				renderer = StarlingParticleHandler(emitterValueObject.emitter.particleHandler).renderer;
				
	            // If this is not called, then Starling can call the render() function of the renderer,
	            // which will try to render with disposed textures.
	            renderer.advanceTime(new Vector.<Particle>);

	            if (renderer.parent)
	            {
	                renderer.parent.removeChild(renderer);
	            }
	        }
	    }
	}
}
