package com.funkypandagame.stardustplayer.emitter
{
	import com.funkypandagame.stardustplayer.Particle2DSnapshot;
	
	import flash.net.registerClassAlias;
	import flash.utils.ByteArray;
	import flash.utils.getQualifiedClassName;
	
	import idv.cjcat.stardustextended.emitters.Emitter;
	import idv.cjcat.stardustextended.handlers.starling.StarlingParticleHandler;
	import idv.cjcat.stardustextended.particles.Particle;
	import idv.cjcat.stardustextended.particles.PooledParticleFactory;
	
	import starling.textures.SubTexture;
	
	public class EmitterValueObject
	{
	    public var emitter:Emitter;
		
	    /** Snapshot of the particles. 
		 * 
		 * If its not null then the emitter will have the particles stored here upon creation.
		 * 
		 * */
	    public var emitterSnapshot:ByteArray;
	
		public static var particleFactory:PooledParticleFactory = new PooledParticleFactory();

	    public function EmitterValueObject( _emitter:Emitter)
	    {
	        emitter = _emitter;
	    }

	    public function get id():String
	    {
	        return emitter.name;
	    }

	    public function get textures() : Vector.<SubTexture>
	    {
	        return StarlingParticleHandler(emitter.particleHandler).textures;
	    }

		private var particlesData:Array;
		private var particles:Vector.<Particle>;
		private var j:int;

	    public function addParticlesFromSnapshot():void
	    {
	        registerClassAlias(getQualifiedClassName(Particle2DSnapshot), Particle2DSnapshot);
	        emitterSnapshot.position = 0;

	        particlesData = emitterSnapshot.readObject();
	        particles = particleFactory.createParticles(particlesData.length, 0);

	        for(j = 0; j < particlesData.length; j++)
			{
	            Particle2DSnapshot(particlesData[j]).writeDataTo(particles[j]);
	        }

	        emitter.addParticles(particles);
	    }
	}
}
