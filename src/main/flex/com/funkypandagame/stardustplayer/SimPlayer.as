package com.funkypandagame.stardustplayer
{
	import com.funkypandagame.stardustplayer.emitter.EmitterValueObject;
	import com.funkypandagame.stardustplayer.project.ProjectValueObject;
	
	import idv.cjcat.stardustextended.handlers.starling.StarlingParticleHandler;
	
	import starling.display.DisplayObjectContainer;
	
	/** Simple class to play back simulations. */
	public class SimPlayer
	{
	    protected var _project:ProjectValueObject;
	    protected var _renderTarget:DisplayObjectContainer;
	
		protected var _scale:Number;

		protected var _x:Number;
		protected var _y:Number;
		
		protected var _isSimulationConfigured:Boolean;

		public function SimPlayer(sim:ProjectValueObject, renderTarget:DisplayObjectContainer)
		{
			_project = sim;
			_renderTarget = renderTarget;
			
			setupSimulation();
		}

		public function set scale(s:Number):void
		{
			if(s != _scale)
			{
				_scale = s;
			}
		}
		
		public function get scale():Number
		{
			return _scale;
		}
			
		public function set x(value:Number):void
		{
			if(_x != value)
			{
				_x = value;
			}
		}
		
		public function get x():Number
		{
			return _x;
		}

		public function set y(value:Number):void
		{
			if(_y != value)
			{
				_y = value;
			}
		}
		
		public function get y():Number
		{
			return _y;
		}

		public function setPos(xp:Number, yp:Number):void
		{
			_x = xp;
			_y = yp;
		}
		
		public function set blendMode(value:String):void
		{
			var handler:StarlingParticleHandler;
			var emitter:EmitterValueObject
			
			for(var i:int = 0, len:int = _project.emitters.length; i < len; i++)
			{
				emitter = _project.emitters[i];
				handler = StarlingParticleHandler(emitter.emitter.particleHandler);
				handler.blendMode = value;
			}
		}
		
	    public function setupSimulation():void
	    {
	        if (_renderTarget == null || _project == null)
	        {
	            return;
	        }

			var handler:StarlingParticleHandler;
			var emitter:EmitterValueObject
			
			if(_isSimulationConfigured == false)
			{
				for(var i:int = 0, len:int = _project.emitters.length; i < len; i++)
				{
					emitter = _project.emitters[i];
					
					handler = StarlingParticleHandler(emitter.emitter.particleHandler); 
					handler.createRendererIfNeeded();
					
					_renderTarget.addChild(handler.renderer);
				}
				
				_isSimulationConfigured = true;
			}
	    }

	    public function getProject():ProjectValueObject
	    {
	        return _project;
	    }
	
		private var handler:StarlingParticleHandler;
		private var emitter:EmitterValueObject;
		private var i:int, len:int;
			
		private static const EXCEPTION_MESSAGE:String = "The simulation and its render target must be set."; 
			
	    public function stepSimulation(deltaTime:Number):void
	    {
	        if(_project == null || _renderTarget == null)
	        {
	            throw new Error(EXCEPTION_MESSAGE);
	        }

	        for(i = 0, len = _project.emitters.length; i < len; i++)
	        {
				emitter = _project.emitters[i];
				emitter.emitter.step(deltaTime);
	        }
	    }
		
		public function resetSimulation():void
		{
			if(_project == null || _renderTarget == null)
			{
				throw new Error(EXCEPTION_MESSAGE);
			}

			var handler:StarlingParticleHandler;

			for(i = 0, len = _project.emitters.length; i < len; i++)
			{
				emitter = _project.emitters[i];
				emitter.emitter.reset();
				
				handler = StarlingParticleHandler(emitter.emitter.particleHandler);
				_renderTarget.removeChild(handler.renderer);
			}
			
			_isSimulationConfigured = false;
		}

		public function dispose():void
		{
			var handler:StarlingParticleHandler;
			var emitter:EmitterValueObject;

			for(var i:int = 0, len:int = _project.emitters.length; i < len; i++)
			{
				emitter = _project.emitters[i];
				handler = StarlingParticleHandler(emitter.emitter.particleHandler);

				_renderTarget.removeChild(handler.renderer);

				handler.renderer.dispose();
			}

			_renderTarget = null;

			_project.destroy();
			_project = null;
			
			_isSimulationConfigured = false;
		}
	}
}
